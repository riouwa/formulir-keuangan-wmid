const filter_oddChar = /^(?!.*\s\s)[^&;<>{}\#`"'!*|:\\\/\f\n\r\t\v\u00a0\u1680\u2000-\u200a\u2028\u2029\u202f\u205f\u3000\ufeff]*$/;
const filter_fullName = /^[A-Z][a-z']{1,}(?: | [A-Z][a-z']*){0,}$/;
const filter_noNumber = /^[^0-9]*$/;
const filter_noDotsCommas = /^[^.,]*$/;
const filter_noBrackets = /^[^\[\]\(\)]*$/;
const filter_kode = /^[A-Z][0-9]{1,2}$/;

$('form.basic-form :required').on('invalid', function() {
    var textfield = $(this).get(0);
    textfield.setCustomValidity('');
    if (!textfield.validity.valid) {
      textfield.setCustomValidity('Harap lengkapi formulir ini');
    }
});

function showPhoto(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
        $('.output-show-' +  input.id).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

function setValAndLock(el, val) {
  var $el = $(el);
  $el.val(val);
  $el.prop("readonly", true);
  $el.prop("tabIndex", "-1");
  $el.css("pointer-events", "none");
}

function getFile($input) {
  return $input.get(0).files[0];
}

function uploadFile($input) {
  var formData = new FormData();
  formData.append("files[]", getFile($input));

  return $.ajax({
    url: "https://mixtape.moe/upload.php",
    data: formData,
    type: "POST",
    contentType: false,
    processData: false,
  })
  .then(function(res){
    return res.files[0].url
  });
}

$(document).ready(function() {
  if ( $( "#user_agent" ).length == 0) {
    alert("Please create user agent for this form!");
  }
  $('input[type=text], input[type=url], input[type=email], input[type=tel], input[type=search]').blur(function(){
    $(this).val($(this).val().trim());
  });
  jQuery.validator.addMethod('oddChar', function(value, element) {
    return (filter_oddChar.test(value));
  }, 'Dilarang menggunakan simbol khusus');
  jQuery.validator.addMethod('noNumber', function(value, element) {
    return (filter_noNumber.test(value));
  }, 'Dilarang memasukan angka');
  jQuery.validator.addMethod('noDotsCommas', function(value, element) {
    return (filter_noDotsCommas.test(value));
  }, 'Dilarang memasukan titik atau koma');
  jQuery.validator.addMethod('noBrackets', function(value, element) {
    return (filter_noBrackets.test(value));
  }, 'Dilarang menggunakan tanda kurung');
  jQuery.validator.addMethod('fullName', function(value, element) {
    return (filter_fullName.test(value));
  }, 'Nama harus dimulai dengan huruf besar');
  jQuery.validator.addMethod('kode', function(value, element) {
    return (filter_kode.test(value));
  }, 'Format kode yang Anda masukan salah');
  $('#user_agent').val(window.navigator.userAgent);
});
